import clothes.*;

public class BossFactory implements Factory{

    public Lippis createLippis() {
        return new BossLippis();
    }

    public Kengät createKengät() {
        return new BossKengät();
    }

    public Tpaita createTpaita() {
        return new BossTpaita();
    }

    public Farmarit createFarmarit() {
        return new BossFarmarit();
    }
}
