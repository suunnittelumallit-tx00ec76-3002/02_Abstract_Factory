import clothes.*;

public class AdidasFactory implements Factory{

    public Lippis createLippis() {
        return new AdidasLippis();
    }

    public Kengät createKengät() {
        return new AdidasKengät();
    }

    public Tpaita createTpaita() {
        return new AdidasTpaita();
    }

    public Farmarit createFarmarit() {
        return new AdidasFarmarit();
    }
}
